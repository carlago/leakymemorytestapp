//
//  RealmProvider.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import Foundation
import Foundation
import RealmSwift

public class RealmProvider {

    var realm: Realm {
        let realmDB = try! Realm()
        return realmDB
    }

    public static var realmFileUrl: URL? {
        return Realm.Configuration.defaultConfiguration.fileURL
    }
}
