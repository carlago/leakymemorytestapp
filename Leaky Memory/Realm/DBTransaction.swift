//
//  DBTransaction.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import Foundation
import RealmSwift

public typealias ExecuteInTransaction = (_ realm: Realm) throws -> Void

public class DBTransaction {

    public let realm: Realm
    private var transactionBlocks = [ExecuteInTransaction]()

    public convenience init() {
        self.init(RealmProvider().realm)
    }

    public init(_ realm: Realm) {
        self.realm = realm
    }

    public init(encapsulate repositories: TransactionRepositoryProtocol...) {
        guard let firstRepo = repositories.first else {
            fatalError("sending in an empty array to encapsulate a DB transaction")
        }
        self.realm = firstRepo.realm
        repositories.forEach { $0.setTransactionalContext(self) }
    }

    public func add(transactionBlock: @escaping ExecuteInTransaction) {
        transactionBlocks.append(transactionBlock)
    }

    public func execute() {
        do {
            try realm.write {
                try transactionBlocks.forEach {
                    try $0(realm)
                }
                transactionBlocks = [ExecuteInTransaction]()
            }
        } catch {
            fatalError("An error occurred while executing transactions in realm")
        }
    }
}
