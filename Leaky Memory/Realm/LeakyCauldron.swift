//
//  LeakyCauldron.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import Foundation
import RealmSwift

public class LeakyCauldron: Object {
    @objc private(set) dynamic var id: String = UUID().uuidString
    @objc private dynamic var startDate: Date = Date.distantPast
    @objc private dynamic var endDate: Date = Date.distantPast
    private(set) public var sourceUUIDs: List<String> = List<String>()

    public convenience init(startDate: AyudaDate, endDate: AyudaDate, sourceUUIDs: [String]) {
        self.init()
        self.startDate = startDate.truncateSeconds().rawDate()
        self.endDate = endDate.truncateSeconds().rawDate()
        self.sourceUUIDs.append(objectsIn: sourceUUIDs)
    }

    public func getStartDate() -> AyudaDate {
        return AyudaDate(date: startDate)
    }

    public func getEndDate() -> AyudaDate {
        return AyudaDate(date: endDate)
    }

    override public class func primaryKey() -> String? {
        return "id"
    }
}
