//
//  LeakyCauldronRepo.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import Foundation
import RealmSwift

public class LeakyCauldronRepo: Repository<LeakyCauldron>, LeakyCauldronRepoProtocol {

    private var cauldronsCache = Results<LeakyCauldron>?

    init() {
        cauldronsCache = self.get()
    }


    public func get(from startDate: AyudaDate, until endDate: AyudaDate) -> [LeakyCauldron] {
        let predicate = NSPredicate(format: "startDate <= %@ AND endDate >= %@",
                                    endDate.truncateSeconds().asCVarArg(),
                                    startDate.truncateSeconds().asCVarArg())

        let results = cauldronsCache?.filter(predicate).toArray()
        return results
    }
}
