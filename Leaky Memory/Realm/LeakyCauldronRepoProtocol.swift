//
//  LeakyCauldronRepoProtocol.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import Foundation
import RealmSwift

public protocol LeakyCauldronRepoProtocol: TransactionRepositoryProtocol {
    func get(from startDate: AyudaDate, until endDate: AyudaDate) -> Results<LeakyCauldron> 
    func add(_ cauldrons: [LeakyCauldron], then: EventCallback?)
}
