//
//  Repository.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import Foundation
import RealmSwift

public typealias EventCallback = () -> Void
public typealias Callback<T> = (T) -> Void

public protocol TransactionRepositoryProtocol {
    var realm: Realm { get }
    func setTransactionalContext(_ transaction: DBTransaction)
}

public protocol RepositoryListenerDelegate: AnyObject {
    func repositoryDidChange()
}

public class Repository<T>: TransactionRepositoryProtocol where T: Object {
    public let realm: Realm
    public var transaction: DBTransaction?

    public convenience init() {
        let provider = RealmProvider()
        self.init(realm: provider.realm)
    }

    required public init(realm: Realm? = nil) {
        let provider = RealmProvider()
        self.realm = realm ?? provider.realm
    }

    public func setTransactionalContext(_ transaction: DBTransaction) {
        self.transaction = transaction
    }

    public func add(_ items: [T], then: EventCallback? = nil) {
        self.upsert(items, then: then)
    }

    public func get() -> Results<T> {
        return realm.objects(T.self)
    }

    public func add(_ item: T, then: EventCallback? = nil) {
        self.upsert(item, then: then)
    }

    public func update(_ item: T, then: EventCallback? = nil) {
        self.upsert(item, update: true, then: then)
    }

    public func update(_ items: [T], then: EventCallback? = nil) {
        self.upsert(items, update: true, then: then)
    }

    public func upsert(_ item: T, update: Bool = false, then: EventCallback? = nil) {
        self.doInTransaction {
            $0.add(item, update: update)
        }
        then?()
    }

    public func upsert(_ items: [T], update: Bool = false, then: EventCallback? = nil) {
        self.doInTransaction {
            for item in items {
                $0.add(item, update: update)
            }
        }
        then?()
    }

    public func removeAll(then: EventCallback? = nil) {
        self.doInTransaction {
            $0.delete($0.objects(T.self))
        }
        then?()
    }

    public func remove(_ items: Results<T>, then: EventCallback? = nil) {
        self.doInTransaction {
            $0.delete(items)
        }
        then?()
    }

    public func subscribeNotification(_ block: @escaping (RealmCollectionChange<Results<T>>,
        [T]) -> Void) -> NotificationToken? {
        let results = realm.objects(T.self)
        let realmToken = results.observe { changes in
            block(changes, results.toArray())
        }
        return realmToken
    }

    public func doInTransaction(executeInTransactionBlock: @escaping (Realm) -> Void) {
        let transaction = self.transaction ?? DBTransaction(realm)

        transaction.add {
            executeInTransactionBlock($0)
        }
        if self.transaction == nil {
            transaction.execute()
        }
    }
}
