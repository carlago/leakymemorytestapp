//
//  AyudaDateComparer.swift
//  AyudaCommon
//
//  Created by Yvette on 21/06/2018.
//  Copyright © 2018 Ayuda Heuristics. All rights reserved.
//

import Foundation

public enum DateSortOrder {
    case earliestToLatest
    case latestToEarliest
}

struct AyudaDateComparer {

    public func findLatestDate(in dates: [AyudaDate]) -> AyudaDate? {
        let ordered = AyudaDateComparer().order(dates: dates, by: .latestToEarliest)
        return ordered.first
    }

    public func findEarliestDate(in dates: [AyudaDate]) -> AyudaDate? {
        let ordered = AyudaDateComparer().order(dates: dates, by: .earliestToLatest)
        return ordered.first
    }

    public func order(dates: [AyudaDate], by sortOrder: DateSortOrder) -> [AyudaDate] {
        var ordered: [AyudaDate] = []
        switch sortOrder {
        case .earliestToLatest:
            ordered = dates.sorted { (date1, date2) -> Bool in
                date1 < date2
            }
        case .latestToEarliest:
            ordered = dates.sorted { (date1, date2) -> Bool in
                date1 > date2
            }
        }
        return ordered
    }

}
