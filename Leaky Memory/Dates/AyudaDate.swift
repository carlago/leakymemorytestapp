//
//  AyudaDate.swift
//  AyudaCommon
//
//  Created by Yvette on 12/04/2018.
//  Copyright © 2018 Ayuda Heuristics. All rights reserved.
//

import Foundation
import UIKit

public typealias DateBounds = (start: AyudaDate, end: AyudaDate)
public typealias TimeBounds = (startTime: Double, endTime: Double?)

let oneHour: TimeInterval = 3600
let oneMinute: TimeInterval = 60

public struct AyudaDate: Hashable {

    private let date: Date

    // Ask yourself if you should REALLY be using this method
    // this should only ever be used when saving the date to the realm object.
    public func rawDate() -> Date {
        return self.date
    }

    // MARK: - Typecasting -
    public func asCVarArg() -> CVarArg {
        return date as CVarArg
    }

    // MARK: - Initializers -
    public init(date: Date = Date()) {
        self.date = date
//        self.calendar.timeZone = TimeZone(abbreviation: "UTC") ?? self.calendar.timeZone
    }

    public init(timeIntervalSinceNow: TimeInterval) {
        self.init(date: Date(timeIntervalSinceNow: timeIntervalSinceNow))
    }

    public init(timeIntervalSince1970: TimeInterval) {
        self.init(date: Date(timeIntervalSince1970: timeIntervalSince1970))
    }

    public func safeDistantPast() -> AyudaDate {
        let dateFormatter = AyudaDateFormatter.customDateFormatter
        let dateString = "2016-01-01T09:00:00" // a date before Ayuda existed
        if let date = dateFormatter.date(from: dateString) {
            return AyudaDate(date: date)
        }
        return AyudaDate(date: Date.distantPast)
    }

    // MARK: - Components -
    public func getMonthAsInt() -> Int {
        let usersCalendar = Calendar.autoupdatingCurrent
        return usersCalendar.component(.month, from: date)
    }

    public func getYearAsInt() -> Int {
        let usersCalendar = Calendar.autoupdatingCurrent
        return usersCalendar.component(.year, from: date)
    }

    // MARK: - Adding Time -
    public func adding(seconds: TimeInterval) -> AyudaDate {
        return AyudaDate(date: date.addingTimeInterval(seconds))
    }

    public func adding(seconds: Int) -> AyudaDate {
        let timeInterval = Double(seconds)
        return self.adding(seconds: timeInterval)
    }

    public func adding(minutes: Int) -> AyudaDate {
        let secondsToAdd: TimeInterval = oneMinute * Double(minutes)
        return self.adding(seconds: secondsToAdd)
    }

    public func adding(hours: Int) -> AyudaDate {
        let secondsToAdd: TimeInterval = oneHour * Double(hours)
        return self.adding(seconds: secondsToAdd)
    }

    public func adding(hours: Double) -> AyudaDate {
        let secondsToAdd: TimeInterval = oneHour * hours
        return self.adding(seconds: secondsToAdd)
    }

    // MARK: - Removing Time -
    public func removing(seconds: TimeInterval) -> AyudaDate {
        return AyudaDate(date: date.addingTimeInterval(-seconds))
    }

    public func removing(minutes: Int) -> AyudaDate {
        let secondsToRemove = oneMinute * Double(minutes)
        return self.removing(seconds: secondsToRemove)
    }

    public func removing(hours: Int) -> AyudaDate {
        let secondsToRemove = oneHour * Double(hours)
        return self.removing(seconds: secondsToRemove)
    }

    public func removing(hours: Double) -> AyudaDate {
        let secondsToRemove: TimeInterval = oneHour * hours
        return self.removing(seconds: secondsToRemove)
    }

    func getUsersTimeZone() -> TimeZone {
        let usersCalendar = Calendar.autoupdatingCurrent
        return usersCalendar.timeZone
    }

    public func getUsersOffsetToUTC() -> Int {
        let usersCalendar = Calendar.autoupdatingCurrent
        return usersCalendar.timeZone.secondsFromGMT()
    }

    // MARK: - Formatting Date -
    public func truncateSeconds() -> AyudaDate {
        var components = calendar().dateComponents([.year, .month, .day, .hour, .minute], from: date)
        components.second = 0

        guard let dateToReturn = calendar().date(from: components) else {
            fatalError("ERROR: date is null!")
        }
        return AyudaDate(date: dateToReturn)
    }

    // MARK: - Comparing -
    public func isSameDay(as dateToCompare: AyudaDate) -> Bool {
        return Calendar.current.compare(dateToCompare.date, to: date, toGranularity: .day) == .orderedSame
    }

    public func isSameMinutes(as dateToCompare: AyudaDate) -> Bool {
        return calendar().compare(dateToCompare.date, to: date, toGranularity: .minute) == .orderedSame
    }

    // MARK: - Elapsed Time -
    public func minutesUntil(_ end: AyudaDate) -> UInt32 {
        if self > end {
            fatalError("Error. End date cannot be before start date")
        }
        return UInt32(end.timeIntervalSince(self) / 60)
    }

    public func minutesFromNow() -> Double {
        let nowWithoutSeconds = AyudaDate().truncateSeconds()
        let timeInterval = nowWithoutSeconds.timeIntervalSince(self)
        return timeInterval/60
    }

    func hoursFromNow() -> CGFloat {
        return CGFloat(date.timeIntervalSinceNow) / CGFloat(oneHour)
    }

    public func timeIntervalSince(_ comparison: AyudaDate) -> Double {
        return date.timeIntervalSince(comparison.date)
    }

    public func timeIntervalSince1970() -> TimeInterval {
        return self.date.timeIntervalSince1970
    }

    // MARK: - Timer -
    public func getNextMinute() -> Date {
        let date = NSDate()
        var components = calendar().dateComponents([.year, .month, .day, .hour, .minute], from: date as Date)
        guard let minutes = components.minute else {
            fatalError("components minute is not present in DateUtils.getNextMinute")
        }
        components.minute = minutes + 1
        components.second = 1
        guard let dateToReturn = calendar().date(from: components) else {
            fatalError("date is null!")
        }
        return dateToReturn
    }

    private func calendar() -> Calendar {
        var calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        calendar.timeZone = TimeZone(abbreviation: "UTC") ?? calendar.timeZone
        return calendar
    }
}

extension AyudaDate {
    // MARK: - String Formatter -

    public func hourMinuteString() -> String {
        return AyudaDateFormatter.getHourMinuteString(from: self.date)
    }

    public func relativeTimeString() -> String {
        return AyudaDateFormatter.relativeTime(for: date)
    }

    public func timeString(relativeTo: AyudaDate) -> String {
        let usersCalendar = Calendar.autoupdatingCurrent
        if let relativeDate = usersCalendar.date(from: usersCalendar.dateComponents([.year, .month, .day],
                                                                           from: relativeTo.date)),
            let dateToBeFormatted = usersCalendar.date(from: usersCalendar.dateComponents([.year, .month, .day],
                                                                             from: self.date)) {
            if dateToBeFormatted.compare(relativeDate) == .orderedAscending {
                let relativeDate = AyudaDateFormatter.getRelativeDateIfRequired(in: self.date, relativeTo: relativeDate,
                        capitalizeFirstLetter: false,
                        todayIsBlank: true)
                if relativeDate == Constants.AyudaString.yesterday {
                    return "\(AyudaDateFormatter.time(for: self.date)) \(relativeDate)"
                } else {
                    return "\(relativeDate) \(AyudaDateFormatter.time(for: self.date))"
                }
            }
        }
        return "\(AyudaDateFormatter.time(for: self.date))"
    }

    public func timeStringFromNow(relativeTo: AyudaDate) -> String {
        let usersCalendar = Calendar.autoupdatingCurrent
        if let relativeDate = usersCalendar.date(from: usersCalendar.dateComponents([.year, .month, .day],
                                                                           from: relativeTo.date)),
            let dateToBeFormatted = usersCalendar.date(from: usersCalendar.dateComponents([.year, .month, .day],
                                                                             from: self.date)) {
            if usersCalendar.isDateInToday(self.date) ||
                       usersCalendar.isDateInYesterday(self.date) ||
                       usersCalendar.isDateInTomorrow(self.date) {
                let relativeDate = AyudaDateFormatter.getRelativeDateIfRequired(in: dateToBeFormatted,
                        relativeTo: relativeDate,
                        capitalizeFirstLetter: false,
                        todayIsBlank: true)
                if relativeDate == Constants.AyudaString.yesterday {
                    return "\(AyudaDateFormatter.time(for: self.date)) \(relativeDate)"
                } else {
                    return "\(relativeDate) \(AyudaDateFormatter.time(for: self.date))"
                }
            } else {
                if dateToBeFormatted.compare(relativeDate) == .orderedAscending ||
                           dateToBeFormatted.compare(relativeDate) == .orderedDescending {
                    let relativeDate = AyudaDateFormatter.getRelativeDateIfRequired(in: self.date,
                            relativeTo: relativeDate,
                            capitalizeFirstLetter: false,
                            todayIsBlank: true)
                    if relativeDate == Constants.AyudaString.yesterday {
                        return "\(AyudaDateFormatter.time(for: self.date)) \(relativeDate)"
                    } else {
                        return "\(relativeDate) \(AyudaDateFormatter.time(for: self.date))"
                    }
                }
            }
        }
        return "\(AyudaDateFormatter.time(for: self.date))"
    }

    public func timeString(toBeDisplayedAt displayDate: AyudaDate) -> String {
        let calendarInUsersTimezone = Calendar.autoupdatingCurrent
        if let displayDateWithoutTime = calendarInUsersTimezone.date(
                from: calendarInUsersTimezone.dateComponents([.year, .month, .day], from: displayDate.date)),
            let dateToBeFormattedWithoutTime = calendarInUsersTimezone.date(
                    from: calendarInUsersTimezone.dateComponents([.year, .month, .day], from: self.date)) {

            let relativeDate = AyudaDateFormatter.getRelativeDateIfRequired(in: dateToBeFormattedWithoutTime,
                                                                            relativeTo: displayDateWithoutTime,
                                                                            capitalizeFirstLetter: false,
                                                                            todayIsBlank: true)
            if relativeDate == Constants.AyudaString.yesterday
                || relativeDate == Constants.AyudaString.tomorrow {
                return "\(AyudaDateFormatter.time(for: self.date)) \(relativeDate)"
            } else if relativeDate == "" {
                return "\(AyudaDateFormatter.time(for: self.date))"
            } else {
                return "\(AyudaDateFormatter.defaultDateFormat(for: date)) \(AyudaDateFormatter.time(for: self.date))"
            }
        }

        return "\(AyudaDateFormatter.defaultDateFormat(for: self.date)) \(AyudaDateFormatter.time(for: self.date))"
    }
    // Returns time only e.g. "2:04 PM"
    public func timeString() -> String {
        return AyudaDateFormatter.time(for: date)
    }

    public func dateString() -> String {
        return AyudaDateFormatter.string(from: date)
    }

    public func absoluteDateString() -> String {
        return AyudaDateFormatter.defaultDateFormat(for: date)
    }

    public func relativeDayAndTimeString() -> String {
        return AyudaDateFormatter.relativeDayAndTimeString(from: date)
    }

    public func relativeDayString() -> String {
        return AyudaDateFormatter.relativeDay(for: date)
    }

    public func getRelativeDateIfRequired(capitalizeFirstLetter: Bool = true,
                                          todayIsBlank: Bool = true) -> String {
        return AyudaDateFormatter.getRelativeDateIfRequired(in: date,
                                                            capitalizeFirstLetter: capitalizeFirstLetter,
                                                            todayIsBlank: todayIsBlank)
    }
}
// MARK: - Extensions -
extension AyudaDate: Equatable {
    public static func == (lhs: AyudaDate, rhs: AyudaDate) -> Bool {
        return lhs.date == rhs.date
    }
}

extension AyudaDate: Comparable {
    public static func < (lhs: AyudaDate, rhs: AyudaDate) -> Bool {
        return lhs.date < rhs.date
    }

    public static func > (lhs: AyudaDate, rhs: AyudaDate) -> Bool {
        return lhs.date > rhs.date
    }
}
