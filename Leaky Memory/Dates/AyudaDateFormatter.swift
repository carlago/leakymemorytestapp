//
//  AyudaDateFormatter.swift
//  Ayuda
//

import Foundation
public class AyudaDateFormatter {

    //locales: https://gist.github.com/jacobbubu/1836273
    //timezones: https://www.timeanddate.com/time/zones/

    // MARK: - Time Stamps -
    static public func longTimeStamp(for date: Date) -> String {
        let timeStamp = longTimeFormatter.string(from: date)
        return timeStamp
    }

    static public func timestamp(for date: Date) -> String {
        let timeStamp = mediumDateShortTimeFormatter.string(from: date)
        return timeStamp
    }

    static public func time(for date: Date) -> String {
        let timeStamp = shortTimeFormatter.string(from: date)
        return timeStamp
    }

    // MARK: - Relative time or day -

    //Returns date and time e.g. Yesterday 2:03 PM or We 14 Mar 2:03 PM
    static public func relativeDayAndTimeString(from date: Date) -> String {
        let timeString = shortTimeFormatter.string(from: date)
        if Calendar.current.isDateInTomorrow(date) {
            return Constants.AyudaString.tomorrowCaps + " " + timeString
        } else if Calendar.current.isDateInYesterday(date) {
            return Constants.AyudaString.yesterdayCaps + " " + timeString
        } else if Calendar.current.isDateInToday(date) {
            return Constants.AyudaString.todayCaps + " " + timeString
        } else {
            return ("\(AyudaDateFormatter.defaultDateFormat(for: date)) \(timeString)")
        }
    }

    //Returns time and date e.g. 2:03 PM yesterday or We 14 Mar 2:03 PM
    static public func relativeTime(for date: Date) -> String {
        let onlyTime = time(for: date)
        if Calendar.current.isDateInToday(date) {
            return "\(onlyTime)"
        } else if Calendar.current.isDateInTomorrow(date) {
            return "\(onlyTime) \(Constants.AyudaString.tomorrow)"
        } else if Calendar.current.isDateInYesterday(date) {
            return "\(onlyTime) \(Constants.AyudaString.yesterday)"
        } else {
            return "\(AyudaDateFormatter.defaultDateFormat(for: date)) \(onlyTime)"
        }
    }

    //Returns date and time e.g. 2:43 PM yesterday
    static public func string(from date: Date) -> String {
        let dateString = shortTimeFormatter.string(from: date)
        if Calendar.current.isDateInTomorrow(date) {
            return dateString + " " + Constants.AyudaString.tomorrow
        } else if Calendar.current.isDateInYesterday(date) {
            return dateString + " " + Constants.AyudaString.yesterday
        } else if Calendar.current.isDateInToday(date) {
            return dateString
        } else {
            return ("\(AyudaDateFormatter.defaultDateFormat(for: date)) \(dateString)")
        }
    }

    //Returns relative day only e.g. "Today" or "We 14 Mar"
    static public func relativeDay(for date: Date) -> String {
        if Calendar.current.isDateInToday(date) {
            return Constants.AyudaString.todayCaps
        } else if Calendar.current.isDateInYesterday(date) {
            return Constants.AyudaString.yesterdayCaps
        } else if Calendar.current.isDateInTomorrow(date) {
            return Constants.AyudaString.tomorrowCaps
        } else {
            return AyudaDateFormatter.defaultDateFormat(for: date)
        }
    }

    static public func getRelativeDateIfRequired(in date: Date,
                                                 capitalizeFirstLetter: Bool = true,
                                                 todayIsBlank: Bool = true) -> String {
        if Calendar.current.isDateInTomorrow(date) {
            return capitalizeFirstLetter ? Constants.AyudaString.tomorrowCaps : Constants.AyudaString.tomorrow
        }
        if Calendar.current.isDateInYesterday(date) {
            return capitalizeFirstLetter ? Constants.AyudaString.yesterdayCaps : Constants.AyudaString.yesterday
        }
        if Calendar.current.isDateInToday(date) {
            return todayIsBlank ? "" : (capitalizeFirstLetter ?
                Constants.AyudaString.todayCaps : Constants.AyudaString.today)
        }
        return AyudaDateFormatter.defaultDateFormat(for: date)
    }

    static public func getRelativeDateIfRequired(in date: Date,
                                                 relativeTo relativeDate: Date,
                                                 capitalizeFirstLetter: Bool = true,
                                                 todayIsBlank: Bool = true) -> String {
        let intervalSince = date.timeIntervalSince(relativeDate)

        // This asummes hour and minutes have been stripped from date and relativeDate
        if intervalSince == 24 * 60 * 60 {
            return capitalizeFirstLetter ? Constants.AyudaString.tomorrowCaps : Constants.AyudaString.tomorrow
        } else if intervalSince == -24 * 60 * 60 {
            return capitalizeFirstLetter ? Constants.AyudaString.yesterdayCaps : Constants.AyudaString.yesterday
        } else if intervalSince == 0 && Calendar.autoupdatingCurrent.isDateInToday(relativeDate) {
            return ""
        } else {
            return AyudaDateFormatter.defaultDateFormat(for: date)
        }
    }

    // MARK: - String From Date -
    // returns 'just now' or 'X hours ago' or 'X minues ago' or 'hh:mm etc' if above six hours
    static func getHourMinuteString(from date: Date) -> String {
        let hoursAgo = AyudaDateFormatter.hoursBetweenNowAnd(date)
        let minutesAgo = AyudaDateFormatter.minutesBetweenNowAnd(date)
        if hoursAgo <= 0 {
            if minutesAgo == 0 {
                return Constants.AyudaString.justNow
            } else {
                let minuteSuffix = minutesAgo > 1 ? Constants.AyudaString.minutes : Constants.AyudaString.minute
                return "\(minutesAgo) \(minuteSuffix) \(Constants.AyudaString.ago)"
            }
        } else if hoursAgo <= 6 {
            let hourSuffix = hoursAgo > 1 ? Constants.AyudaString.hours : Constants.AyudaString.hour
            return "\(hoursAgo) \(hourSuffix) \(Constants.AyudaString.ago)"
        } else {
            return AyudaDateFormatter.string(from: date)
        }
    }

    // MARK: - Private -
    private static func hoursBetweenNowAnd(_ date: Date) -> Int {
        let secondsInHour: Double = 60 * 60
        return Int(Date().timeIntervalSince(date)/secondsInHour)
    }

    private static func minutesBetweenNowAnd(_ date: Date) -> Int {
        let secondsInMinute: Double = 60
        return Int(Date().timeIntervalSince(date)/secondsInMinute)
    }

    public static func defaultDateFormat(for date: Date) -> String {
        let currentDate = Date()
        let timeDifference = abs(currentDate.timeIntervalSince1970 - date.timeIntervalSince1970)
        let oneYear: Double = 60 * 60 * 24 * 365
        var dateFormat = ""
        if timeDifference > oneYear {
            dateFormat = "EEEEEE d MMM yyyy"
        } else {
            dateFormat = "EEEEEE d MMM"
        }
        let df = createDateFormatter(dateFormat: dateFormat, doesRelativeDateFormatting: false)
        return df.string(from: date)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc
    static private func didLocaleChange(_ notification: Notification) {
        if let dateFormatter = notification.object as? DateFormatter {
            dateFormatter.locale = NSLocale.current
        }
    }

    static private func registerLocaleChangeNotification(for dateFormatter: DateFormatter) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didLocaleChange),
                                               name: NSLocale.currentLocaleDidChangeNotification,
                                               object: nil)
    }

}

// MARK: - DATE FORMATTERS -
extension AyudaDateFormatter {

    public static var shortTimeFormatter: DateFormatter = {
        return createDateFormatter(dateStyle: .none, timeStyle: .short)
    }()

    /// dateformatter with style - "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
    public static var customDateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        df.doesRelativeDateFormatting = false
        df.locale = NSLocale.autoupdatingCurrent
        df.timeZone = TimeZone.autoupdatingCurrent
        return df
    }()

    /// FOR PERSISTENCE! dateformatter with UTC timezone style - "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
    public static var UTC24HRDateFormatter: DateFormatter = {
        let df = DateFormatter()
        let aTwentyFourHoursLocale = Locale(identifier: "en_GB")
        df.dateFormat = DateFormatter.dateFormat(fromTemplate: "yyyy'-'MM'-'dd'T'HH':'mm':'ss",
                                                 options: 0,
                                                 locale: aTwentyFourHoursLocale)
        df.doesRelativeDateFormatting = false
        return df
    }()

    //Private
    private static var mediumDateShortTimeFormatter: DateFormatter = {
        return createDateFormatter(dateStyle: .medium, timeStyle: .short)
    }()

    private static var mediumDateFormatter: DateFormatter = {
        return createDateFormatter(dateStyle: .medium, timeStyle: .none)
    }()

    private static var longTimeFormatter: DateFormatter = {
        return createDateFormatter(dateStyle: .long, timeStyle: .short)
    }()

    private static func createDateFormatter(dateStyle: DateFormatter.Style,
                                            timeStyle: DateFormatter.Style) -> DateFormatter {
        let df = DateFormatter()
        df.dateStyle = dateStyle
        df.timeStyle = timeStyle
        df.doesRelativeDateFormatting = true
        df.locale = NSLocale.autoupdatingCurrent
        df.timeZone = TimeZone.autoupdatingCurrent
        return df
    }

    private static func createDateFormatter(dateFormat: String,
                                            doesRelativeDateFormatting: Bool) -> DateFormatter {

        let df = DateFormatter()
        df.dateFormat = dateFormat
        df.doesRelativeDateFormatting = doesRelativeDateFormatting
        df.locale = NSLocale.autoupdatingCurrent
        df.timeZone = TimeZone.autoupdatingCurrent
        return df
    }
}

public struct Constants {
    public struct AyudaString {
        static let selectTime = "select time"
        static let ago = "ago"
        static let and = " and "
        static let iTook = "I took "
        static let iAteA = "I ate a "
        static let minute = "minute"
        static let hour = "hour"
        static let iUse = "I use "
        static let justNow = "just now"
        static let tomorrow = "tomorrow"
        public static let yesterday = "yesterday"
        static let yesterdayCaps = "Yesterday"
        static let today = "today"
        static let todayCaps = "Today"
        static let hours = "hours"
        static let minutes = "minutes"
        static let newsToMe = "News to me."
        static let goodRefresher = "Good refresher."
        static let empty = ""
        static let singleSpace = " "
        static let tomorrowCaps = "Tomorrow"
        static let done = "Done"
        static let add = "Add"
        static let now = "Now"
        static let deleteResponse = "Delete Response"
        static let cancel = "Cancel"
        static let edit = "Edit"
    }
}
