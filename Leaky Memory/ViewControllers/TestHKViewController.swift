//
//  ViewController.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import UIKit
import HealthKit

private var type = HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!

class TestHKViewController: UIViewController, HealthKitDataSourceDelegate {
    private var statsQuery: HKStatisticsCollectionQuery?
    var counter: Int = 0

    @IBOutlet weak var infoLabel: UILabel!


    @IBAction func kickOffTapped(_ sender: Any) {
        kickOff()
    }

    private func kickOff() {
        var datasource: HealthKitDataSource? = HealthKitDataSource()
        datasource?.delegate = self
        datasource?.kickOff()
        datasource = nil
    }

    func queryStarted(predicateStart: Date, predicateEnd: Date) {
        self.infoLabel.text = "Starting Stats Query \n\n date predicate: \n \(predicateStart) \n \(predicateEnd) \n\n waiting..."
    }

    func queryCompleted(completionTime seconds: Double) {
        self.infoLabel.text = "Stats Query Completed in \(seconds) seconds"
    }

}

struct HealthKitDataTypes {
    let dataTypes: [HKQuantityTypeIdentifier] = [.distanceWalkingRunning, .bloodGlucose]

    var dataTypeSet: Set<HKObjectType> {
        return dataTypes.reduce(Set<HKObjectType>()) { accumulator, dataType in
            var set = accumulator
            if let objectType = HKObjectType.quantityType(forIdentifier: dataType) {
                set.insert(objectType)
            }
            return set
        }
    }
}

protocol HealthKitDataSourceDelegate {
    func queryStarted(predicateStart: Date, predicateEnd: Date)
    func queryCompleted(completionTime seconds: Double)
}

struct HealthKitDataSource {
    private let healthStore = HKHealthStore()
    public var delegate: HealthKitDataSourceDelegate?

    public func kickOff() {
        requestHealthKitAccess(completion: { _,_ in
            self.statisticsQuery(start: Date.distantPast, end: Date())
        })
    }

    private func statisticsQuery(start: Date,
                                 end: Date) {
        DispatchQueue.main.async {
            self.delegate?.queryStarted(predicateStart: start, predicateEnd: end)
        }

        let startQueryTime = Date()
        var interval = DateComponents()
        interval.minute = 10

        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: queryPredicateWithPadding(start: start, end: end),
                                                options: [.cumulativeSum, .separateBySource],
                                                anchorDate: Date(),
                                                intervalComponents: interval)

        query.initialResultsHandler = { (query, collection, error) in
            let end = Date()
            let diff = end.timeIntervalSince(startQueryTime)
            DispatchQueue.main.async {
                self.delegate?.queryCompleted(completionTime: diff)
            }
            print("completed in: \(diff) seconds")
            self.healthStore.stop(query)
        }
        print("execute stats query")
        healthStore.execute(query)
    }

    private func queryPredicateWithPadding(start: Date, end: Date) -> NSCompoundPredicate {
        
        let datePredicate = HKQuery.predicateForSamples(withStart: start,
                                                        end: end,
                                                        options: [])

        var queryPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [])

        queryPredicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [datePredicate])

        print("Converter: StatsQuery predicate: \(start) - \(end)")
        return queryPredicate
    }

    private func requestHealthKitAccess(completion: @escaping (Bool, Error?) -> Void) {
        healthStore.requestAuthorization(toShare: nil, read: HealthKitDataTypes().dataTypeSet) { success, error in
            if let error = error {
                NSLog("error occurred while trying to request authorisation from HealthKit \(error)")
            }
            completion(success, error)
        }
    }
}

