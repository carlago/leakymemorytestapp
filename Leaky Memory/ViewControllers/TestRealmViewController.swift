//
//  TestRealmViewController.swift
//  Leaky Memory
//
//  Created by Carla Goldstein on 04/09/2018.
//  Copyright © 2018 Ayuda Heuristics Ltd. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class TestRealmViewController: UIViewController {
    var repo = LeakyCauldronRepo()
    var realm: Realm?

    @IBOutlet weak var infoTableView: UITableView!
    private var info: [String] = []
    var counter: Int = 0
    var cauldrons: Results<LeakyCauldron>?

    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()

        let start = AyudaDate().removing(hours: 1000)
        let end = AyudaDate().adding(hours: 1000)


        // Query Realm for all dogs less than 2 years old

        cauldrons = repo.get(from: start, until: end)

        infoTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        infoTableView.dataSource = self
    }

    @IBAction func kickOff1Tapped(_ sender: Any) {
        info.insert("ADD 1 CAULDRON", at: 0)
        let lc = LeakyCauldron(startDate: AyudaDate(), endDate: AyudaDate(), sourceUUIDs: ["test", "one", "two", "three"])
        repo.add(lc)
        infoTableView.reloadData()
    }

    @IBAction func loopReadTapped(_ sender: Any) {
        info.insert("LOOP READ: cauldron count: \(cauldronCount())", at: 0)
        infoTableView.reloadData()
    }

    @IBAction func bulkReadTapped(_ sender: Any) {
        info.insert("BULK READ: cauldron count: \(cauldronCount())", at: 0)
        infoTableView.reloadData()
    }

    @IBAction func kickOff200tapped(_ sender: Any) {
        info.insert("ADD 200 CAULDRON", at: 0)
        var toAdd: [LeakyCauldron] = []
        for _ in 1...200 {
            toAdd.append(LeakyCauldron(startDate: AyudaDate(), endDate: AyudaDate(), sourceUUIDs: ["test", "one", "two", "three"]))
        }
        try! realm?.write {
            realm?.add(toAdd)
        }
        infoTableView.reloadData()
    }

    private func cauldronCount() -> Int {
        return cauldrons?.count ?? 0
    }
}

extension TestRealmViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return info.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = info[indexPath.row]
        return cell
    }
}

extension Results {
    public func toArray() -> [Results.Iterator.Element] {
        var array = [Results.Iterator.Element]()
        for item in self {
            array.append(item)
        }
        return array
    }
}
